public class BlueDevil extends Person {


	public BlueDevil(String name, String country, String hobby, String sex, String major, String undergrad) {
		super(name, country, hobby, sex);
		this.major=major;
		this.undergrad=undergrad;
		

	}
	
	private String major;
	private String undergrad;

	public String toString()
	{
		String str=super.toString();
		if(sex=="male")
        {
            str+="He ";
        }
        else
        {
            str+="She ";
		}
		
		str+="graduated from "+undergrad + " and now is majoring in "+ major+".";
		return str;
	}
      
}
