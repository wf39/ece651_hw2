import java.util.*;

public class TestMain {

	public static void whoIs(Vector<Person> dukepeople, String Name)
	{
		for (int i=0; i<dukepeople.size(); i++)
		{
			if (dukepeople.elementAt(i).name==Name)
			{
				System.out.println(dukepeople.elementAt(i).toString());
			}
		}
	}
	public static void main(String[] args) {
		Vector<Person> dukepeople = new Vector<Person> ();
		dukepeople.add(new BlueDevil("Wenkai Fan", "China", "modeling", "male", "Physics", "Nanjing University"));
		dukepeople.add(new BlueDevil("Adel Fahmy", "US", "tennis, biking, gardening, and cooking", "male", "Electrical and Computer Engineering", "North Carolina State University"));
		dukepeople.add(new BlueDevil("Zhongyu Li", "China", "playing basketball", "male", "Electrical and Computer Engineering", "Southeast University"));
		whoIs(dukepeople, "Wenkai Fan");
		whoIs(dukepeople, "Adel Fahmy");
		whoIs(dukepeople, "Zhongyu Li");
	}

}
