public class Person {
    String name;
    String country;
    String hobby;
    String sex;

    public Person(String name, String country, String hobby, String sex)
    {
        this.name=name;
        this.country=country;
        this.hobby=hobby;
        this.sex=sex;
    }

    public String toString()
    {
        String str=name+" is from "+country+". ";
        if(sex=="male")
        {
            str+="He ";
        }
        else
        {
            str+="She ";
        }
        str+="enjoys "+ hobby+". ";
        return str;
    }
}

